<?php

/**
 * @file
 * Template file for Image source formatter.
 *
 * Available variables:
 * - $source_attribute: The source attribute.
 * - $style_url: The URL to the image.
 * - $img_class: The added class to the image.
 */
?>

<img <?php print $source_attribute?>="<?php print $style_url?>" class="img-source-formatter <?php (!empty($img_class)) ? print $img_class : NULL;?>" >

