﻿INTRODUCTION
------------

This module allows you to change the image data source attribute to "data-lazy" or "data-src" for some markup purposes or other functionality requirements.

Note: Image may not display if you change the source attribute to "data-lazy" or "data-src". This will just provide an image markup with the data source that you've selected on the settings.


INSTRUCTION
-----------

1.) Install and enable this module.
2.) Go to "Manage display" on the image that you want to change the data source.
3.) Set up the formatter to "Image source".
4.) Change the settings to what you wanted to be applied to the image.
5.) Then Save.


MAINTAINERS
-----------

Carlo Miguel Agno (carlagno) - https://www.drupal.org/user/3317429
